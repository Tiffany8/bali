import { promises as fs } from "fs";
import * as path from "path";

export interface Config {
  aliasesFiles: string[];
}

const CONFIG_FILE = path.resolve(__dirname, "config.json");

export const loadConfig = async (): Promise<Config> => {
  try {
    const data = await fs.readFile(CONFIG_FILE, "utf8");
    return JSON.parse(data) as Config;
  } catch (error) {
    return { aliasesFiles: ["~/.bash_aliases"] };
  }
};

export const saveConfig = async (config: Config) => {
  const data = JSON.stringify(config, null, 2);
  await fs.writeFile(CONFIG_FILE, data, "utf8");
};

export interface Alias {
  name: string;
  fullAlias: string;
}

export const searchAliases = async (filePaths: string[], query: string): Promise<Alias[]> => {
  const aliases: Alias[] = [];
  for (const filePath of filePaths) {
    const expandedPath = path.resolve(filePath.replace("~", process.env.HOME || ""));
    try {
      const data = await fs.readFile(expandedPath, "utf8");
      const fileAliases = data
        .split("\n")
        .filter(line => line.startsWith("alias") && line.includes(query))
        .map(line => {
          const aliasStartIndex = line.indexOf("alias ") + 6; // Start after "alias "
          const aliasEndIndex = line.indexOf("=");
          const aliasName = line.substring(aliasStartIndex, aliasEndIndex).trim();
          const fullAlias = line.substring(aliasStartIndex).trim(); // Remove the initial "alias " part
          return { name: aliasName, fullAlias };
        });
      aliases.push(...fileAliases);
    } catch (error) {
      console.error(`Failed to read file: ${expandedPath}`, error)
      throw error
    }
  }
  return aliases;
};

export const addAlias = async (filePaths: string[], aliasName: string, command: string) => {
  for (const filePath of filePaths) {
    const expandedPath = path.resolve(filePath.replace("~", process.env.HOME || ""));
    try {
      const data = await fs.readFile(expandedPath, "utf8");
      const aliases = data.split("\n").filter(line => line.startsWith("alias"));

      // Check for existing alias or command
      for (const line of aliases) {
        const aliasStartIndex = line.indexOf("alias ") + 6;
        const aliasEndIndex = line.indexOf("=");
        const existingAliasName = line.substring(aliasStartIndex, aliasEndIndex).trim();
        const existingCommand = line.substring(aliasEndIndex + 1).trim().replace(/^['"]|['"]$/g, ""); // Remove quotes around command

        if (existingAliasName === aliasName) {
          throw new Error(`Alias name "${aliasName}" already exists in file "${filePath}".`);
        }

        if (existingCommand === command) {
          throw new Error(`Command "${command}" is already assigned to another alias in file "${filePath}".`);
        }
      }

      const alias = `alias ${aliasName}='${command}'\n`;
      await fs.appendFile(expandedPath, alias, "utf8");
      
      // Provide the command to source the file
      return `source ${expandedPath}`;
    } catch (error) {
      console.error(`Failed to read/write file: ${expandedPath}`, error)
      throw error
    }
  }
};

export const removeAlias = async (filePaths: string[], aliasName: string) => {
  for (const filePath of filePaths) {
    const expandedPath = path.resolve(filePath.replace("~", process.env.HOME || ""));
    try {
      const data = await fs.readFile(expandedPath, "utf8");
      const newData = data
        .split("\n")
        .filter(line => !line.startsWith(`alias ${aliasName}=`))
        .join("\n");
      await fs.writeFile(expandedPath, newData, "utf8");
    } catch (error) {
      console.error(`Failed to read/write file: ${expandedPath}`, error)
      throw error
    }
  }
};
