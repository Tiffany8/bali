import { useState, useEffect } from "react";
import { Action, ActionPanel, Icon, List, Toast, showToast } from "@raycast/api";
import { Alias, Config, loadConfig, removeAlias, searchAliases } from "./utils";
import { showFailureToast } from "@raycast/utils";

const SearchAliases = () => {
  const [aliases, setAliases] = useState<Alias[]>([]);
  const [query, setQuery] = useState("")
  const [config, setConfig] = useState<Config| null>(null)

  useEffect(() => {
    loadConfig().then(setConfig).catch(error => {
      showFailureToast(error, {title: "Failed to load configuration"})
    })
  }, [])

  useEffect(() => {
    if (!config) return;  // Do nothing if config isn't loaded

    const fetchAliases = async () => {
        const result = await searchAliases(config.aliasesFiles, query);
        setAliases(result);
    };

    fetchAliases().catch(error => {
        showFailureToast(error, {title: "Failed to load aliases"});
    });
}, [query, config])

const handleRemoveAlias = async (aliasName: string) => {
  if (!config) {
      showToast(Toast.Style.Failure, 'Configuration is not loaded');
      return;
  }

  removeAlias(config.aliasesFiles, aliasName)
    .then(() => {
      showToast(Toast.Style.Success, '🧹 Alias removed');
      setAliases(prev => prev.filter(alias => alias.name !== aliasName));
    })
    .catch(error => {
        showFailureToast(error, {title: "Failed to remove alias"});
    });
  }

  return (
    <List
      searchBarPlaceholder="Search Aliases"
      onSearchTextChange={setQuery}
      isLoading={aliases.length === 0 && query === ""}
    >
      {aliases.map((alias, index) => (
        <List.Item
          key={index}
          title={alias.fullAlias}
          actions={
            <ActionPanel title="BAli">
              <Action.CopyToClipboard content={alias.name} />
              <Action icon={Icon.Trash} title="Remove Alias" onAction={() => handleRemoveAlias(alias.name)}/>
            </ActionPanel >
          }
        />
      ))}
    </List>
  );
};

export default SearchAliases;
