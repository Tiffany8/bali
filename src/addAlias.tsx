import { useState } from "react";
import { Form, ActionPanel, Action, showToast, Toast } from "@raycast/api";
import { FormValidation, showFailureToast, useForm } from "@raycast/utils"

import { addAlias, loadConfig } from "./utils";
interface ManageAliasesValues {
  aliasName: string
  command: string
}

const AddAlias = () => {
  const [aliasName, setAliasName] = useState("")
  const [command, setCommand] = useState("")
  const [sourceCommand, setSourceCommand] = useState("")

  const { handleSubmit, itemProps, reset } = useForm<ManageAliasesValues>({
    onSubmit(values) {
        handleAddAlias(values.aliasName, values.command).then(
          () => {
            showToast({
              style: Toast.Style.Success,
              title: "🥳",
              message: `alias ${values.aliasName}='${values.command}' created`,
            })
          }
        ).catch(
          (error) => {
            showFailureToast(error, {title: "😭"})
          }
        )
    },
    validation: {
      aliasName: FormValidation.Required,
      command: FormValidation.Required,
    }
  })

  const handleAddAlias = async (aliasName: string, command: string) => {
      const config = await loadConfig()
      const cmd = await addAlias(config.aliasesFiles, aliasName, command)
      setSourceCommand(cmd || "")
  }

  return (
    <Form
    navigationTitle="Add Alias ✨"
    actions={
      <ActionPanel>
        <Action.SubmitForm title="Add Alias" onSubmit={handleSubmit} />
      </ActionPanel>
    }>
      <Form.TextField
        title="Alias Name"
        value={aliasName}
        placeholder="e.g. docs"
        onChange={setAliasName}
        {...itemProps.aliasName}
      />
      <Form.TextField
        title="Command"
        value={command}
        placeholder="e.g. cd ~/Documents"
        onChange={setCommand}
        {...itemProps.command}
      />
      {sourceCommand && (
        <Form.Description 
          text={sourceCommand} 
          title=" Run the following command to source the alias file:"
        />
      )}
    </Form>
  );
}

export default AddAlias