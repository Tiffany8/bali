// src/RemoveAlias.tsx

import { useState } from "react";
import { Form, ActionPanel, Action, showToast, Toast } from "@raycast/api";
import { removeAlias, loadConfig } from "./utils";
import { showFailureToast } from "@raycast/utils";

const RemoveAlias = () => {
  const [aliasName, setAliasName] = useState("");

  const handleRemoveAlias = async () => {
    try {
      const config = await loadConfig();
      await removeAlias(config.aliasesFiles, aliasName);
      showToast(Toast.Style.Success, `{🧹} Alias removed`)
    } catch (error) {
      showFailureToast('😭', {title: "Failed to remove alias"});
    }
  };

  return (
    <Form>
      <Form.TextField
        id="aliasName"
        title="Alias Name"
        value={aliasName}
        onChange={setAliasName}
      />
      <ActionPanel>
        <Action title="Remove Alias" onAction={handleRemoveAlias} />
      </ActionPanel>
    </Form>
  );
};

export default RemoveAlias;
